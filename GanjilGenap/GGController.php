<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GGController extends Controller
{
    public function ganjilgenap(Request $request){
	$bilanganAwal = $request->input('bilanganAwal');
	$bilanganAkhir = $request->input('bilanganAkhir');
	$hasil = "";
	if($bilanganAwal < $bilanganAkhir){
    	for($i=$bilanganAwal; $i<=$bilanganAkhir; $i++){
        	    if($i % 2 > 0){
					
					$hasil .= 
					"<table border='1' cellpadding='10'>"
					."<tr>"
					."<td>"."Angka ".$i."</td>"
					."<td>"."Adalah Ganjil"."</td>"
					."</tr>"
					."</table>";

            	}else{
            		if($i % 2 == 0){
						$hasil .=
						"<table border='1' cellpadding='10'>"
					."<tr>"
					."<td>"."Angka ".$i."</td>"
					."<td>"."Adalah Genap"."</td>"
					."</tr>"
					."</table>";
            		}
        		}
    		}
		}
		echo "<h3>"."Bilangan yang anda masukan adalah ".$bilanganAwal. " dan " . $bilanganAkhir. " hasilnya adalah : "."<br>"."<br>";
		echo $hasil;		
	}
}
