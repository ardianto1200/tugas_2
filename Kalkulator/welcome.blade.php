<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <!-- <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css"> -->
        <link href="{{asset('css/app.css')}}" rel="stylesheet">
        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #0000;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
            .row{
                margin-top:30px;
            }

        </style>
    </head>
    <body>
        <div class="container">
            <form action="kalkulator" method="get">
            
            <div class="row">
                <div class="col-md-3">
                   <input type="number" name="bil1" class="form-control" placeholder ="Masukan Angka" required> 
                </div>

                <div class="col-md-3">
                   <input type="number" name="bil2" class="form-control" placeholder ="Masukan Angka" required> 
                </div>

                <div class="col-md-3">
                  <select name="operasi" class="form-control">
                    <option value="tambah">+</option>
                    <option value="kurang">-</option>
                    <option value="bagi">/</option>
                    <option value="kali">X</option>
                  </select>
                </div>

                <div class="col-md-3">
                   <button type="submit" class="btn btn-info">Hasil
                   </button>
                </div>
            </div>
            </form>

            <div class="row">
                <div class="col-md-2">

                </div>

                <div class="col-md-6">
                    @if(session('info'))
                    <div class="alert alert-info">
                        {{session('info')}}
                    </div>
                     @endif
                </div>

                <div class="col-md-2">

                </div>

            </div>
        </div>
    </body>
</html>
