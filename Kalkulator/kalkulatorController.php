<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class kalkulatorController extends Controller
{
    public function kalkulatorFunc(Request $request){
        $operasi = $request->input('operasi');
        $bil_1 = $request->input('bil1');
        $bil_2 = $request->input('bil2');
        $result = 0;

        if($operasi == "tambah"){
			$result = $bil_1+$bil_2;
		}elseif($operasi == "kurang"){
			$result = $bil_1-$bil_2;
		}elseif($operasi == "kali"){
			$result = $bil_1*$bil_2;
		}elseif($operasi == "bagi"&&$bil_2!=0){	
			$result =$bil_1/$bil_2;
		}
		else{
			$result = "Tidak Bisa Dilakukan";
        }
        return redirect ('/')->with('info', 'Hasilnya adalah : '. $result);
        
    }
}
